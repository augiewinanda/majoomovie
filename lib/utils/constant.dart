class Preference {
  static const IS_LOGGED_IN = "is_logged_in";
  static const USER_VALUE = "user_value";
}

class Api {
  static const BASE_URL =
      "https://imdb8.p.rapidapi.com/auto-complete?q=fastfurious";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
