import 'dart:convert';

import 'package:majootestcase/database/query/user_query.dart';
import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart' as sqlite;
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart' as path;

class DbHelper {
  // Create Table
  final tables = [
    UserQuery.CREATE_TABLE,
  ];

  Future<Database> openDB() async {
    final dbPath = await sqlite.getDatabasesPath();
    return sqlite.openDatabase(path.join(dbPath, 'testcasemajoo.db'),
        onCreate: (db, version) {
      tables.forEach((table) async {
        await db.execute(table).then((value) {
          print("berhasil ");
        }).catchError((err) {
          print("errornya ${err.toString()}");
        });
      });
      print('Table Created');
    }, version: 1);
  }

  Future<String> loginUser(User user) async {
    final db = await openDB();
    var _result = await db.rawQuery(
        "SELECT * FROM users WHERE email = ? AND password = ?",
        [user.email, user.password]);
    var result = _result.toList();

    if (result.length == 0) {
      return null;
    } else {
      return jsonEncode(result[0]);
    }
  }

  Future<String> registerUser(User user) async {
    final db = await openDB();
    var _result = await db.rawQuery(
        "SELECT * FROM users WHERE username = ? OR email = ?",
        [user.username, user.email]);
    var result = _result.toList();

    if (result.length != 0) {
      return null;
    } else {
      await db.transaction((txn) async {
        await txn.rawInsert(
            "INSERT INTO users(username, email, password) VALUES(?, ?, ?)",
            [user.username, user.email, user.password]);
      });

      return jsonEncode(user);
    }
  }
}
