// To parse this JSON data, do
//
//     final movieResponse = movieResponseFromJson(jsonString);

import 'dart:convert';

MovieResponse movieResponseFromJson(String str) =>
    MovieResponse.fromJson(json.decode(str));

String movieResponseToJson(MovieResponse data) => json.encode(data.toJson());

class MovieResponse {
  MovieResponse({
    this.data,
    this.query,
    this.v,
  });

  List<Data> data;
  String query;
  int v;

  factory MovieResponse.fromJson(Map<String, dynamic> json) => MovieResponse(
        data: json["d"] == null
            ? null
            : List<Data>.from(json["d"].map((x) => Data.fromJson(x))),
        query: json["q"] == null ? null : json["q"],
        v: json["v"] == null ? null : json["v"],
      );

  Map<String, dynamic> toJson() => {
        "d": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "q": query == null ? null : query,
        "v": v == null ? null : v,
      };
}

class Data {
  Data({
    this.i,
    this.id,
    this.l,
    this.s,
    this.q,
    this.rank,
    this.v,
    this.vt,
    this.y,
    this.yr,
  });

  I i;
  String id;
  String l;
  String s;
  String q;
  int rank;
  List<V> v;
  int vt;
  int y;
  String yr;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        i: json["i"] == null ? null : I.fromJson(json["i"]),
        id: json["id"] == null ? null : json["id"],
        l: json["l"] == null ? null : json["l"],
        s: json["s"] == null ? null : json["s"],
        q: json["q"] == null ? null : json["q"],
        rank: json["rank"] == null ? null : json["rank"],
        v: json["v"] == null
            ? null
            : List<V>.from(json["v"].map((x) => V.fromJson(x))),
        vt: json["vt"] == null ? null : json["vt"],
        y: json["y"] == null ? null : json["y"],
        yr: json["yr"] == null ? null : json["yr"],
      );

  Map<String, dynamic> toJson() => {
        "i": i == null ? null : i.toJson(),
        "id": id == null ? null : id,
        "l": l == null ? null : l,
        "s": s == null ? null : s,
        "q": q == null ? null : q,
        "rank": rank == null ? null : rank,
        "v": v == null ? null : List<dynamic>.from(v.map((x) => x.toJson())),
        "vt": vt == null ? null : vt,
        "y": y == null ? null : y,
        "yr": yr == null ? null : yr,
      };
}

class I {
  I({
    this.imageUrl,
    this.height,
    this.width,
  });

  String imageUrl;
  int height;
  int width;

  factory I.fromJson(Map<String, dynamic> json) => I(
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        height: json["height"] == null ? null : json["height"],
        width: json["width"] == null ? null : json["width"],
      );

  Map<String, dynamic> toJson() => {
        "imageUrl": imageUrl == null ? null : imageUrl,
        "height": height == null ? null : height,
        "width": width == null ? null : width,
      };
}

class V {
  V({
    this.i,
    this.id,
    this.l,
    this.s,
  });

  I i;
  String id;
  String l;
  String s;

  factory V.fromJson(Map<String, dynamic> json) => V(
        i: json["i"] == null ? null : I.fromJson(json["i"]),
        id: json["id"] == null ? null : json["id"],
        l: json["l"] == null ? null : json["l"],
        s: json["s"] == null ? null : json["s"],
      );

  Map<String, dynamic> toJson() => {
        "i": i == null ? null : i.toJson(),
        "id": id == null ? null : id,
        "l": l == null ? null : l,
        "s": s == null ? null : s,
      };
}
