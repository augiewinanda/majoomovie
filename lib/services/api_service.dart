import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/constant.dart';

class ApiServices {
  DioError _error;

  DioError get error => _error;

  Future<MovieResponse> getMovieList() async {
    try {
      var dio = await dioConfig.dio(Api.BASE_URL);
      Response<String> response = await dio.get(Api.BASE_URL);

      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data));

      return movieResponse;
    } catch (e) {
      _error = e;

      return null;
    }
  }
}
