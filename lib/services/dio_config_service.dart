import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance;

createInstance(String baseUrl) async {
  var options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "13a5ca5893msh191b8969d900ef4p16842djsnd686fb012ae2",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(
    PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90,
    ),
  );
}

Future<Dio> dio(String baseUrl) async {
  await createInstance(baseUrl);
  dioInstance.options.baseUrl = baseUrl;
  return dioInstance;
}
