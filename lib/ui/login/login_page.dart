import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/theme/palette_color.dart';
import 'package:majootestcase/theme/spacing_dimens.dart';
import 'package:majootestcase/theme/typography_style.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocErrorState) {
            Fluttertoast.showToast(
                msg: state.error.toString(),
                toastLength: Toast.LENGTH_SHORT,
            );
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: SpacingDimens.spacing44,
                left: SpacingDimens.spacing24,
                right: SpacingDimens.spacing24,
                bottom: SpacingDimens.spacing44,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Selamat Datang',
                    style: TypographyStyle.title.merge(
                      TextStyle(
                        fontWeight: FontWeight.w800,
                        color: PaletteColor.primary,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing12,
                  ),
                  Text(
                    'Senang bertemu dengan Anda. Masukkan Username '
                    'dan Password untuk mengakses aplikasi Majoo Movie.',
                    style: TypographyStyle.paragraph.merge(
                      TextStyle(
                        fontWeight: FontWeight.w600,
                        color: PaletteColor.grey80,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing36,
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/images/Illustration-login.png',
                      width: 200,
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing36,
                  ),
                  _form(),
                  SizedBox(
                    height: SpacingDimens.spacing48,
                  ),
                  CustomButton(
                    text: 'LOGIN',
                    onPressed: handleLogin,
                    height: 55.0,
                  ),
                  _register(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val) ? null : 'Masukkan E-Mail yang valid!';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            hint: 'Password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: PaletteColor.grey60,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (_) => BlocProvider(
                create: (context) => RegisterBlocCubit(),
                child: RegisterPage(),
              ),
            ),
          );
        },
        child: RichText(
          text: TextSpan(
            text: 'Belum punya akun? ',
            style: TypographyStyle.paragraph.merge(
              TextStyle(color: PaletteColor.grey80),
            ),
            children: [
              TextSpan(
                text: 'Daftar',
                style: TextStyle(
                  color: PaletteColor.primary,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit = context.read<AuthBlocCubit>();
      User user = User(
        email: _email,
        password: _password,
      );
      authBlocCubit.login_user(user);
    }
  }
}
