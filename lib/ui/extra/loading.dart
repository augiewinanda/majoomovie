import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:majootestcase/theme/palette_color.dart';

class LoadingIndicator extends StatelessWidget {
  final double height;
  final Color color;

  const LoadingIndicator({
    Key key,
    this.height = 10,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitFadingCircle(
          size: 45,
          itemBuilder: (BuildContext context, int index) {
            return DecoratedBox(
              decoration: BoxDecoration(
                color: PaletteColor.primary,
                shape: BoxShape.circle,
              ),
            );
          },
        ),
      ),
    );
  }
}
