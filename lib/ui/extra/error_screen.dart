import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/theme/spacing_dimens.dart';
import 'package:majootestcase/theme/typography_style.dart';

class ErrorScreen extends StatelessWidget {
  final String title;
  final String message;
  final Function() retry;
  final Color textColor;
  final double fontSize;
  final double gap;
  final Widget retryButton;

  const ErrorScreen(
      {Key key,
      this.gap = 10,
      this.retryButton,
      this.title = "",
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SpacingDimens.spacing36,
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/Illustration-error.png',
                  width: 200,
                ),
              ),
              SizedBox(
                height: SpacingDimens.spacing64,
              ),
              Text(
                title,
                style: TypographyStyle.subtitle1.merge(
                  TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              SizedBox(
                height: SpacingDimens.spacing16,
              ),
              Text(
                message,
                style: TypographyStyle.subtitle2,
              ),
              retry != null
                  ? Column(
                      children: [
                        SizedBox(
                          height: 60,
                        ),
                        retryButton ??
                            CustomButton(
                              text: "RETRY",
                              onPressed: () {
                                if (retry != null) retry();
                              },
                            ),
                      ],
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
