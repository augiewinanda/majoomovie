import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/theme/palette_color.dart';
import 'package:majootestcase/theme/spacing_dimens.dart';
import 'package:majootestcase/theme/typography_style.dart';
import 'package:skeleton_text/skeleton_text.dart';

class DetailMovie extends StatelessWidget {
  final Data data;

  DetailMovie({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: false,
            snap: false,
            floating: true,
            expandedHeight: 270.0,
            backgroundColor: PaletteColor.primary,
            flexibleSpace: Stack(
              children: [
                CachedNetworkImage(
                  imageUrl: data.i == null
                      ? "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                      : data.i.imageUrl,
                  width: double.infinity,
                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  progressIndicatorBuilder: (context, url, downloadProgress) {
                    return SkeletonAnimation(
                      borderRadius: BorderRadius.circular(4.0),
                      // shimmerDuration: 1000,
                      child: Container(
                        // height: 80,
                        decoration: BoxDecoration(
                          color: PaletteColor.grey40,
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(
                top: SpacingDimens.spacing24,
                left: SpacingDimens.spacing16,
                right: SpacingDimens.spacing16,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.l == null ? "" : data.l,
                    style: TypographyStyle.title2.merge(
                      TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing8,
                  ),
                  Row(
                    children: [
                      Text(
                        data.y == null ? "" : data.y.toString(),
                        style: TypographyStyle.caption1.merge(
                          TextStyle(
                            color: PaletteColor.primary,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Text(
                        data.s == null ? "" : "  -  " + data.s,
                        style: TypographyStyle.caption1,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing16,
                  ),
                  _description(),
                  SizedBox(
                    height: SpacingDimens.spacing28,
                  ),
                  Text(
                    "List Series",
                    style: TypographyStyle.title2.merge(
                      TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing16,
                  ),
                  data.v != null
                      ? Container(
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: data.v == null ? 0 : data.v.length,
                            itemBuilder: (context, index) {
                              return seriesItemWidget(
                                data.v[index],
                              );
                            },
                          ),
                        )
                      : Container(
                          child: Text(
                            'No Movie Series',
                            style: TypographyStyle.caption2,
                          ),
                        ),
                  SizedBox(
                    height: SpacingDimens.spacing64,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget seriesItemWidget(V v) {
    return Container(
      width: 250,
      margin: EdgeInsets.only(
        right: SpacingDimens.spacing16,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        border: Border.all(
          color: PaletteColor.grey40,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 7,
            child: Container(
              decoration: BoxDecoration(
                color: PaletteColor.primarybg2,
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: CachedNetworkImage(
                imageUrl: v.i.imageUrl == null
                    ? "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                    : v.i.imageUrl,
                width: double.infinity,
                fit: BoxFit.cover,
                errorWidget: (context, url, error) => Icon(Icons.error),
                progressIndicatorBuilder: (context, url, downloadProgress) {
                  return SkeletonAnimation(
                    borderRadius: BorderRadius.circular(4.0),
                    // shimmerDuration: 1000,
                    child: Container(
                      // height: 80,
                      decoration: BoxDecoration(
                        color: PaletteColor.grey40,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(
                SpacingDimens.spacing12,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    v.l == null ? "" : v.l,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TypographyStyle.paragraph.merge(
                      TextStyle(
                        color: PaletteColor.grey,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _description() {
    return Text(
      "Lorem Ipsum is simply dummy text of the printing and "
      "typesetting industry. Lorem Ipsum has been the "
      "industry's standard dummy text ever since the 1500s, "
      "when an unknown printer took a galley of type and "
      "scrambled it to make a type specimen book. It has "
      "survived not only five centuries, but also the leap "
      "into electronic typesetting, remaining essentially "
      "unchanged. It was popularised in the 1960s with the "
      "release of Letraset sheets containing Lorem Ipsum "
      "passages, and more recently with desktop publishing "
      "software like Aldus PageMaker including versions of "
      "Lorem Ipsum.\n\nIt is a long established fact that a "
      "reader will be distracted by the readable content of "
      "a page when looking at its layout. The point of using "
      "Lorem Ipsum is that it has a more-or-less normal "
      "distribution of letters, as opposed to using 'Content "
      "here, content here', making it look like readable "
      "English. Many desktop publishing packages and web page "
      "editors now use Lorem Ipsum as their default model text, "
      "and a search for 'lorem ipsum' will uncover many web "
      "sites still in their infancy. Various versions have "
      "evolved over the years, sometimes by accident, "
      "sometimes on purpose (injected humour and the like).\n\n"
      "There are many variations of passages of Lorem Ipsum "
      "available, but the majority have suffered alteration "
      "in some form, by injected humour, or randomised words "
      "which don't look even slightly believable. If you are "
      "going to use a passage of Lorem Ipsum, you need to be "
      "sure there isn't anything embarrassing hidden in the "
      "middle of text. All the Lorem Ipsum generators on the "
      "Internet tend to repeat predefined chunks as necessary,"
      " making this the first true generator on the Internet. "
      "It uses a dictionary of over 200 Latin words, combined "
      "with a handful of model sentence structures, to generate "
      "Lorem Ipsum which looks reasonable. The generated Lorem "
      "Ipsum is therefore always free from repetition, injected "
      "humour, or non-characteristic words etc.",
      style: TypographyStyle.paragraph,
    );
  }
}
