import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/theme/palette_color.dart';
import 'package:majootestcase/theme/spacing_dimens.dart';
import 'package:majootestcase/theme/typography_style.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  final _passwordRepeatController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  bool _isObscurePasswordRepeat = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegisterBlocCubit, RegisterBlocState>(
        listener: (context, state) {
          if (state is RegisterBlocErrorState) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  state.error.toString(),
                ),
              ),
            );
          } else if (state is RegisterBlocLoggedInState) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: SpacingDimens.spacing44,
                left: SpacingDimens.spacing24,
                right: SpacingDimens.spacing24,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Daftar Akun',
                    style: TypographyStyle.title.merge(
                      TextStyle(
                        fontWeight: FontWeight.w800,
                        color: PaletteColor.primary,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing12,
                  ),
                  Text(
                    'Daftarkan identitas kamu sekarang, agar dapat bergabung menjadi '
                    'bagian Majoo Indonesia.',
                    style: TypographyStyle.paragraph.merge(
                      TextStyle(
                        fontWeight: FontWeight.w600,
                        color: PaletteColor.grey80,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing16,
                  ),
                  _form(),
                  SizedBox(
                    height: SpacingDimens.spacing48,
                  ),
                  CustomButton(
                    text: 'REGISTER',
                    onPressed: handleRegister,
                    height: 55.0,
                  ),
                  _login(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _login(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (_) => BlocProvider(
                create: (context) => AuthBlocCubit(),
                child: LoginPage(),
              ),
            ),
          );
        },
        child: RichText(
          text: TextSpan(
            text: 'Sudah punya akun? ',
            style: TypographyStyle.paragraph.merge(
              TextStyle(color: PaletteColor.grey80),
            ),
            children: [
              TextSpan(
                text: 'Login',
                style: TextStyle(
                  color: PaletteColor.primary,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Username',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'E-Mail',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val) ? null : 'Masukkan E-Mail yang valid!';
              } else {
                return '';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            hint: 'Password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: PaletteColor.grey60,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            hint: 'Konfirmasi Password',
            controller: _passwordRepeatController,
            isObscureText: _isObscurePasswordRepeat,
            validator: (val) {
              if (val != _passwordController.value) {
                return "konfirmasi password salah";
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePasswordRepeat
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: PaletteColor.grey60,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePasswordRepeat = !_isObscurePasswordRepeat;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _username = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null) {
      RegisterBlocCubit registerBlocCubit = context.read<RegisterBlocCubit>();
      User user = User(
        username: _username,
        email: _email,
        password: _password,
      );
      registerBlocCubit.register_user(user);
    }
  }
}
