import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/theme/palette_color.dart';
import 'package:majootestcase/theme/spacing_dimens.dart';
import 'package:majootestcase/theme/typography_style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:provider/provider.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data> data;
  final User user;

  const HomeBlocLoadedScreen({
    Key key,
    this.data,
    this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Majoo Movie",
          style: TypographyStyle.subtitle1.merge(
            TextStyle(
              color: PaletteColor.primarybg,
              // fontWeight: FontWeight.w700,
            ),
          ),
        ),
        actions: [
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              showDialog(
                context: context,
                builder: (_) => _showDialogUser(
                  context,
                ),
              );
            },
            child: Icon(
              Icons.person,
            ),
          ),
        ],
        backgroundColor: PaletteColor.primary,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: SpacingDimens.spacing12,
          horizontal: SpacingDimens.spacing12,
        ),
        child: GridView.builder(
          itemCount: data.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 12,
            mainAxisSpacing: 12,
            childAspectRatio: 2.4 / 3.4,
          ),
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailMovie(
                      data: data[index],
                    ),
                  ),
                );
              },
              child: movieItemWidget(
                data[index],
                context,
              ),
            );
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(Data data, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        border: Border.all(
          color: PaletteColor.grey40,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 7,
            child: Container(
              decoration: BoxDecoration(
                color: PaletteColor.primarybg2,
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: CachedNetworkImage(
                imageUrl: data.i == null
                    ? "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                    : data.i.imageUrl,
                width: double.infinity,
                fit: BoxFit.cover,
                errorWidget: (context, url, error) => Icon(
                  Icons.image_not_supported_outlined,
                ),
                progressIndicatorBuilder: (context, url, downloadProgress) {
                  return SkeletonAnimation(
                    borderRadius: BorderRadius.circular(4.0),
                    // shimmerDuration: 1000,
                    child: Container(
                      // height: 80,
                      decoration: BoxDecoration(
                        color: PaletteColor.grey40,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(
                SpacingDimens.spacing8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.l == null ? "" : data.l,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TypographyStyle.paragraph.merge(
                      TextStyle(
                        color: PaletteColor.grey,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SpacingDimens.spacing4,
                  ),
                  Text(
                    data.y == null ? "" : data.y.toString(),
                    style: TypographyStyle.mini.merge(
                      TextStyle(
                        color: PaletteColor.primary,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showDialogUser(BuildContext context) {
    return AlertDialog(
      title: Text(
        'User Information',
        style: TypographyStyle.title2,
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            user.username == null ? "" : user.username,
            style: TypographyStyle.subtitle2,
          ),
          SizedBox(
            height: SpacingDimens.spacing8,
          ),
          Text(
            user.email == null ? "" : user.email,
            style: TypographyStyle.subtitle2,
          ),
          SizedBox(
            height: SpacingDimens.spacing36,
          ),
          CustomButton(
            text: "Logout",
            onPressed: () {
              Navigator.pop(context);

              HomeBlocCubit homeBlocCubit = context.read<HomeBlocCubit>();
              homeBlocCubit.user_logout();
            },
          ),
        ],
      ),
    );
  }
}
