import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/utils/error_helper.dart';
import 'home_bloc_loaded_screen.dart';
import 'package:provider/provider.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
      builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return HomeBlocLoadedScreen(
            data: state.data,
            user: state.user,
          );
        } else if (state is HomeBlocLoadingState) {
          return LoadingIndicator();
        } else if (state is HomeBlocInitialState) {
          return Scaffold();
        } else if (state is HomeBlocUserLogout) {
          return BlocProvider(
            create: (context) => AuthBlocCubit()..fetch_history_login(),
            child: MyHomePageScreen(),
          );
        } else if (state is HomeBlocErrorState) {
          DioError error = state.error;
          ErrorHelper errorHelper = ErrorHelper();
          HomeBlocCubit homeBlocCubit = context.read<HomeBlocCubit>();

          if (error.error is SocketException) {
            return ErrorScreen(
              title: "Something Wrong!",
              message: errorHelper.extractApiError(
                error,
              ),
              retry: () => homeBlocCubit.fetching_data(),
            );
          } else {
            return ErrorScreen(
              message: errorHelper.extractApiError(
                error,
              ),
            );
          }
        }

        return Center(
            child: Text(kDebugMode ? "state not implemented $state" : ""));
      },
    );
  }
}
