part of 'home_bloc_cubit.dart';

abstract class HomeBlocState extends Equatable {
  const HomeBlocState();

  @override
  List<Object> get props => [];
}

class HomeBlocInitialState extends HomeBlocState {}

class HomeBlocLoadingState extends HomeBlocState {}

class HomeBlocLoadedState extends HomeBlocState {
  final List<Data> data;
  final User user;

  HomeBlocLoadedState(this.data, this.user);

  @override
  List<Object> get props => [data, user];
}

class HomeBlocUserProfile extends HomeBlocState {
  final data;

  HomeBlocUserProfile(this.data);

  @override
  List<Object> get props => [data];
}

class HomeBlocUserLogout extends HomeBlocState {}

class HomeBlocErrorState extends HomeBlocState {
  final error;

  HomeBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
