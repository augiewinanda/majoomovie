import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetching_data() async {
    emit(HomeBlocLoadingState());

    ApiServices apiServices = ApiServices();
    MovieResponse movieResponse = await apiServices.getMovieList();

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userData = sharedPreferences.getString(Preference.USER_VALUE);

    if (movieResponse == null) {
      emit(HomeBlocErrorState(apiServices.error));
    } else {
      List<User> user = userFromJson("[" + userData + "]");

      emit(HomeBlocLoadedState(movieResponse.data, user[0]));
    }
  }

  void user_logout() async {
    emit(HomeBlocUserLogout());

    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }
}
