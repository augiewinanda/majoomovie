import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState());

  void register_user(User user) {
    emit(RegisterBlocInitialState());

    final DbHelper _helper = DbHelper();

    _helper.registerUser(user).then((value) async {
      if (value == null) {
        emit(RegisterBlocErrorState("Username/Email telah digunakan"));
      } else {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        await sharedPreferences.setBool(Preference.IS_LOGGED_IN, true);
        String data = value.toString();
        sharedPreferences.setString(Preference.USER_VALUE, data);
        emit(RegisterBlocLoggedInState());
      }
    });
  }
}
