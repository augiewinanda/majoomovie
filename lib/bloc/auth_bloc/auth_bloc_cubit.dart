import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async {
    // emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async {
    emit(AuthBlocLoginState());

    final DbHelper _helper = DbHelper();

    _helper.loginUser(user).then((value) async {
      if (value == null) {
        emit(AuthBlocErrorState("Login Gagal, Periksa kembali inputan anda!"));
      } else {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        await sharedPreferences.setBool(Preference.IS_LOGGED_IN, true);
        String data = value.toString();
        sharedPreferences.setString(Preference.USER_VALUE, data);
        emit(AuthBlocLoggedInState());
      }
    });
  }
}
